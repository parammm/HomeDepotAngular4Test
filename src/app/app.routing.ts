import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { ToolsComponent } from './tools.component';

const appRoutes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'tools',
        component: ToolsComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);