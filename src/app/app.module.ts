import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent }  from './app.component';
import { routing } from './app.routing';
import { HomeComponent } from './home.component';
import { ToolsComponent } from './tools.component';
import { CurrencyConverterComponent } from './currency-converter.component';

@NgModule({
  imports: [ BrowserModule, NgbModule.forRoot(), routing ],
  declarations: [ AppComponent, HomeComponent, ToolsComponent, CurrencyConverterComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }